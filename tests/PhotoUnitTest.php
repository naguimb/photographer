<?php

namespace App\Tests;

use App\Entity\Photo;
use App\Entity\Category;
use App\Entity\User;
use DateTime;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class PhotoUnitTest extends TestCase
{
    public function testIsTrue()
    {

        $photo = new Photo();
        $datetime = new DateTime();
        $datetimeImmutable = new DateTimeImmutable();
        $category = new Category();
        $user = new User();

        $photo->setName('name')
            ->setWidth(20.20)
            ->setHeight(20.20)
            ->setOnSale(true)
            ->setReleaseDate($datetime)
            ->setCreatedAt($datetimeImmutable)
            ->setDescription('description')
            ->setPortfolio(true)
            ->setSlug('slug')
            ->setFile('file')
            ->addCategory($category)
            ->setPrice(20.20)
            ->setUser($user);

        $this->assertTrue($photo->getName() === 'name');
        $this->assertTrue($photo->getWidth() == 20.20);
        $this->assertTrue($photo->getHeight() == 20.20);
        $this->assertTrue($photo->getOnSale() === true);
        $this->assertTrue($photo->getReleaseDate() === $datetime);
        $this->assertTrue($photo->getCreatedAt() === $datetimeImmutable);
        $this->assertTrue($photo->getDescription() === 'description');
        $this->assertTrue($photo->getPortfolio() === true);
        $this->assertTrue($photo->getSlug() === 'slug');
        $this->assertTrue($photo->getFile() === 'file');
        $this->assertContains($category, $photo->getCategory());
        $this->assertTrue($photo->getPrice() == 20.20);
        $this->assertTrue($photo->getUser() === $user);
    }

    public function testIsFalse()
    {
        $photo = new Photo();
        $datetime = new DateTime();
        $datetimeImmutable = new DateTimeImmutable();
        $category = new Category();
        $user = new User();

        $photo->setName('name')
            ->setWidth(20.20)
            ->setHeight(20.20)
            ->setOnSale(true)
            ->setReleaseDate($datetime)
            ->setCreatedAt($datetimeImmutable)
            ->setDescription('description')
            ->setPortfolio(true)
            ->setSlug('slug')
            ->setFile('file')
            ->addCategory($category)
            ->setPrice(20.20)
            ->setUser($user);

        $this->assertFalse($photo->getName() === 'false');
        $this->assertFalse($photo->getWidth() == 21.20);
        $this->assertFalse($photo->getHeight() == 21.20);
        $this->assertFalse($photo->getOnSale() === false);
        $this->assertFalse($photo->getReleaseDate() === new DateTime());
        $this->assertFalse($photo->getCreatedAt() === new DateTimeImmutable());
        $this->assertFalse($photo->getDescription() === 'false');
        $this->assertFalse($photo->getPortfolio() === false);
        $this->assertFalse($photo->getSlug() === 'false');
        $this->assertFalse($photo->getFile() === 'false');
        $this->assertNotContains(new Category(), $photo->getCategory());
        $this->assertFalse($photo->getPrice() == 21.20);
        $this->assertFalse($photo->getUser() === new User());
    }

    public function testIsEmpty()
    {
        $photo = new photo();

        $this->assertEmpty($photo->getName());
        $this->assertEmpty($photo->getWidth());
        $this->assertEmpty($photo->getHeight());
        $this->assertEmpty($photo->getOnSale());
        $this->assertEmpty($photo->getReleaseDate());
        $this->assertEmpty($photo->getCreatedAt());
        $this->assertEmpty($photo->getDescription());
        $this->assertEmpty($photo->getPortfolio());
        $this->assertEmpty($photo->getSlug());
        $this->assertEmpty($photo->getFile());
        $this->assertEmpty($photo->getCategory());
        $this->assertEmpty($photo->getPrice());
        $this->assertEmpty($photo->getUser());
    }
}
