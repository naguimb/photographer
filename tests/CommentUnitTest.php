<?php

namespace App\Tests;

use App\Entity\Comment;
use App\Entity\Blogpost;
use App\Entity\Photo;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class CommentUnitTest extends TestCase
{
    public function testIsTrue()
    {

        $comment = new Comment();
        $blogpost = new Blogpost();
        $photo = new Photo();
        $datetimeImmutable = new DateTimeImmutable();

        $comment->setAuthor('author')
            ->setEmail('email@test.com')
            ->setContent('content')
            ->setCreatedAt($datetimeImmutable)
            ->setBlogpost($blogpost)
            ->setPhoto($photo);

        $this->assertTrue($comment->getAuthor() === 'author');
        $this->assertTrue($comment->getEmail() === 'email@test.com');        
        $this->assertTrue($comment->getContent() === 'content');
        $this->assertTrue($comment->getCreatedAt() === $datetimeImmutable);
        $this->assertTrue($comment->getBlogpost() === $blogpost);
        $this->assertTrue($comment->getPhoto() === $photo);
    }

    public function testIsFalse()
    {
        $comment = new Comment();
        $blogpost = new Blogpost();
        $photo = new Photo();
        $datetimeImmutable = new DateTimeImmutable();

        $comment->setAuthor('false')
            ->setEmail('false@test.com')
            ->setContent('false')
            ->setCreatedAt(new DateTimeImmutable())
            ->setBlogpost(new Blogpost())
            ->setPhoto(new Photo());

        $this->assertFalse($comment->getAuthor() === 'author');
        $this->assertFalse($comment->getEmail() === 'email@test.com');        
        $this->assertFalse($comment->getContent() === 'content');
        $this->assertFalse($comment->getCreatedAt() === $datetimeImmutable);
        $this->assertFalse($comment->getBlogpost() === $blogpost);
        $this->assertFalse($comment->getPhoto() === $photo);
    }


    public function testIsEmpty()
    {
        $comment = new Comment();

        $this->assertEmpty($comment->getAuthor());
        $this->assertEmpty($comment->getEmail());        
        $this->assertEmpty($comment->getContent());
        $this->assertEmpty($comment->getCreatedAt());
        $this->assertEmpty($comment->getBlogpost());
        $this->assertEmpty($comment->getPhoto());
    }
}
