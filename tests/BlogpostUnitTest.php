<?php

namespace App\Tests;

use App\Entity\Blogpost;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class BlogpostUnitTest extends TestCase
{
    public function testIsTrue()
    {

        $blogpost = new Blogpost();
        $datetimeImmutable = new DateTimeImmutable();

        $blogpost->setTitle('title')
            ->setContent('content')
            ->setSlug('slug')
            ->setCreatedAt($datetimeImmutable);

        $this->assertTrue($blogpost->getTitle() === 'title');
        $this->assertTrue($blogpost->getContent() === 'content');
        $this->assertTrue($blogpost->getSlug() === 'slug');
        $this->assertTrue($blogpost->getCreatedAt() === $datetimeImmutable);

    }

    public function testIsFalse()
    {
        $blogpost = new Blogpost();
        $datetimeImmutable = new DateTimeImmutable();

        $blogpost->setTitle('title')
            ->setContent('content')
            ->setSlug('slug')
            ->setCreatedAt($datetimeImmutable);

        $this->assertFalse($blogpost->getTitle() === 'false');
        $this->assertFalse($blogpost->getContent() === 'false');
        $this->assertFalse($blogpost->getSlug() === 'false');
        $this->assertFalse($blogpost->getCreatedAt() === new DatetimeImmutable);
    }

    public function testIsEmpty()
    {
        $blogpost = new Blogpost();

        $this->assertEmpty($blogpost->getTitle());
        $this->assertEmpty($blogpost->getContent());
        $this->assertEmpty($blogpost->getSlug());
        $this->assertEmpty($blogpost->getCreatedAt());
    }
}
