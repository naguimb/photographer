<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new User();

        $user->setEmail('true@test.com')
             ->setFirstName('firstname')
             ->setLastName('lastname')
             ->setPassword('password')
             ->setAbout('about')
             ->setInstagram('instagram');

        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getFirstName() === 'firstname');
        $this->assertTrue($user->getLastName() === 'lastname');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getAbout() === 'about');
        $this->assertTrue($user->getInstagram() === 'instagram');
    }

    public function testIsFalse()
    {
        $user = new User();

        $user->setEmail('true@test.com')
             ->setFirstName('firstname')
             ->setLastName('lastname')
             ->setPassword('password')
             ->setAbout('about')
             ->setInstagram('instagram');

        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getFirstName() === 'false');
        $this->assertFalse($user->getLastName() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getAbout() === 'false');
        $this->assertFalse($user->getInstagram() === 'false');
    }  
    
    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getEmail() === '');
        $this->assertEmpty($user->getFirstName() === '');
        $this->assertEmpty($user->getLastName() === '');
        $this->assertEmpty($user->getPassword() === '');
        $this->assertEmpty($user->getAbout() === '');
        $this->assertEmpty($user->getInstagram() === '');
    }    
}
